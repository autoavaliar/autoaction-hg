<?php

declare(strict_types=1);

namespace AutoAction\Hg\Translate;

/**
 * Lista de strings padrao para testes
 *
 * @package AutoAction\Hg\Translate
 * @date    30/04/2020 15:02
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class HgLocalStringSample implements HgLocalStringsInterface
{
    public function getStrings(): array
    {
        return [
            'empresa' => 'Empresa Sample',
            'marca'   => 'Marca Sample',
            'modelo'  => 'Modelo Sample',
            'versao'  => 'Versao Sample',
        ];
    }
}