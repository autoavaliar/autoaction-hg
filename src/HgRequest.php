<?php

declare(strict_types=1);

namespace AutoAction\Hg;

use AutoAction\Hg\HttpRequest\HttpRequest;
use Exception;

/**
 * HG Request
 *
 * @package AutoAction\Hg
 * @date    29/04/2020 13:39
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class HgRequest
{
    /**
     * @var HgConfig
     */
    private $config;

    /**
     * @param HgConfig $config
     */
    public function __construct(HgConfig $config)
    {
        $this->config = $config;
    }

    public function getConfig(): HgConfig
    {
        return $this->config;
    }

    /**
     * Efetua uma requisição para o serviço
     *
     * @param string $endpoint
     * @param array  $data
     * @param array  $headers
     *
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $endpoint, array $data, array $headers = [], $method = 'POST'):array
    {

        return (new HttpRequest($this->config->getHost()))->request($endpoint, $data, $headers, $method);
    }

}