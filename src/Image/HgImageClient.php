<?php

declare(strict_types=1);

namespace AutoAction\Hg\Image;

use AutoAction\Hg\HttpRequest\HttpRequest;
use AutoAction\Hg\Message\LoggerStack;
use Exception;

/**
 * Cliente HG image
 *
 * @package AutoAction\Hg
 *
 */
class HgImageClient
{
    const WATERMARK_ENDPOINT = "/add-watermark-to-image";
    private $httpRequest;
    private $token;

    public function __construct(string $token, HttpRequest $httpRequest)
    {
        $this->token = $token;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @throws Exception
     */
    public function addWatermark(array $data = [], array $header = []): array
    {
        return $this->request(self::WATERMARK_ENDPOINT, ['images' => $data], $header);
    }

    /**
     * @throws Exception
     */
    public function request(string $endpoint, array $data = [], array $header = [], $method = 'POST'): array
    {
        try {
            $header = array_merge(['token' => $this->getToken()], $header);

            return $this->httpRequest->request($endpoint, $data, $header, $method);
        } catch (Exception $e) {
            LoggerStack::addException($e);
            throw $e;
        }
    }

    private function getToken(): string
    {
        return $this->token;
    }
}