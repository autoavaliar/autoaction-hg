<?php

declare(strict_types=1);

namespace AutoAction\Hg\Translate;

use AutoAction\Hg\HgClient;
use AutoAction\Hg\Message\LoggerStack;
use Exception;

/**
 * Tradução HG
 *
 * @package AutoAction\Hg\Translate
 * @date    30/04/2020 11:23
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class HgTranslate
{
    /**
     * @var HgTranslateConfig
     */
    private $config;

    /**
     * @var HgClient
     */
    private $client;

    /**
     * @var HgLocalStringsInterface
     */
    private $localStrings;

    private static $strings;

    private static $instance;

    /**
     * @param HgTranslateConfig       $config       Configurações da tradução
     * @param HgClient                $client       Client HG
     * @param HgLocalStringsInterface $localStrings Lista de strings local
     */
    private function __construct(HgTranslateConfig $config, HgClient $client, HgLocalStringsInterface $localStrings)
    {
        $this->config       = $config;
        $this->client       = $client;
        $this->localStrings = $localStrings;
    }

    public static function getInstance(
        HgTranslateConfig $config,
        HgClient $client,
        HgLocalStringsInterface $localStrings
    ): self {
        if (!is_null(self::$instance)) {
            return self::$instance;
        }
        self::$instance = new self($config, $client, $localStrings);

        return self::$instance;
    }

    private function getCacheKey()
    {
        if(!empty($this->config->getInstanceId())) {
            return $this->client->getConfig()->getCacheKey('translate:system-' . $this->config->getSystemId() . ':instance-' . $this->config->getInstanceId());
        }

        return $this->client->getConfig()->getCacheKey('translate:system-' . $this->config->getSystemId());
    }

    /**
     * Lista de strings local
     */
    private function getLocalStrings(): array
    {
        if (is_null($this->localStrings)) {
            throw new Exception('Local Strings not found!', E_USER_ERROR);
        }

        return $this->localStrings->getStrings();
    }

    private function getFromAPI()
    {
        try {
            $params = [
                'systemId'    => $this->config->getSystemId(),
                'showData'    => 1,
                'forceReload' => 0,
            ];

            $instanceId = $this->config->getInstanceId();
            if($instanceId) {
               $params['instanceId'] = $instanceId;
            }

            $data = $this->client->post('translate/get', $params);

            if (!is_array($data)) {
                throw new Exception('Error fetching the translation from the API!', E_USER_ERROR);
            }

            $data = array_column($data, 'text', 'root');

            LoggerStack::addTrack(['getTranslateFromAPI' => true]);
        } catch (Exception $e) {
            LoggerStack::addException($e);
            return [];
        }

        return $data;
    }

    private function getStrings(): array
    {
        $key   = $this->getCacheKey();
        $cache = $this->client->getConfig()->getCache();

        if ($this->config->isDeleteCache()) {
            $cache->delete($key);
        }

        if ($cache->exists($key)) {
            return $cache->get($key);
        }

        $data = $this->getFromAPI();
        if (empty($data)) {
            $this->config->setTtl(30);
            $data = $this->getLocalStrings();
            LoggerStack::addTrack(['getTranslateFromLocal' => true]);
        }

        $cache->save($key, $data, $this->config->getTtl());

        return $data;
    }

    public function get($root)
    {
        if (is_null(self::$strings)) {
            self::$strings = $this->getStrings();
        }

        $root = trim($root);

        $strings = self::$strings;

        // verifica a existencia da chave na lista de strings
        if (!isset($strings[$root])) {
            $strings = $this->getLocalStrings();
        }

        if (!isset($strings[$root])) {
            return $root;
        }

        return $strings[$root];
    }

}