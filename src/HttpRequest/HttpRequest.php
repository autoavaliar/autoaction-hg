<?php
declare(strict_types=1);

namespace AutoAction\Hg\HttpRequest;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class HttpRequest
{
    private $host;
    private $checkStatusCode = true;

    /**
     * @throws Exception
     */
    public function __construct(string $host)
    {
        $this->setHost($host);
    }

    public function noCheckStatusCode()
    {
        $this->checkStatusCode = false;
    }

    /**
     * Efetua uma requisição para o serviço
     * @throws Exception|GuzzleException
     */
    public function request(string $endpoint, array $data, array $headers = [], $method = 'POST'):array
    {
        $body = [
            'json' => $data
        ];

        $options = [
            'headers' => array_merge(
                [
                    'Content-Type' => 'application/json'
                ],
                $headers
            )
        ];

        $url = sprintf('%s/%s', $this->getHost(), $endpoint);

        // efetuação da requisição
        $client  = new Client(['http_errors' => false, 'verify' => false]);
        $request = new Request($method, $url);
        $send    = $client->send($request, array_merge($options, $body));
        return $this->response($send);
    }

    /**
     * Monta a estrutura de resposta do serviço
     * @throws Exception
     */
    private function response(ResponseInterface $response): array
    {
        if ($response->getStatusCode() != 200 && $this->checkStatusCode) {
            throw new Exception("Could not query information!", E_USER_WARNING);
        }

        $content = json_decode($response->getBody()->getContents(), true);
        if (!$content['status']) {
            throw new Exception(
                $content['messages'][0]['error_message'] ??
                "Could not query information!",
                E_USER_WARNING
            );
        }

        return $content['payload'];
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function setHost(string $host)
    {
        if (filter_var($host, FILTER_VALIDATE_URL) === FALSE) {
            throw new Exception(
                $content['messages'][0]['error_message'] ?? "Not a valid URL",
                E_USER_WARNING
            );
        }
        $this->host = $host;
        return $this;
    }
}