<?php

declare(strict_types=1);

namespace AutoAction\Hg\Message;

use Throwable;

/**
 * Stack de logs
 *
 * @package AutoAction\Hg\Message
 * @date    30/04/2020 13:58
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class LoggerStack
{
    public static $logTrack = [];
    public static $errorTrack = [];

    public static function addTrack(array $data)
    {
        self::$logTrack = array_merge(self::$logTrack, $data);
    }

    public static function addErrorTrack(array $data)
    {
        self::$errorTrack = array_merge(self::$errorTrack, $data);
    }

    public static function getTrack(): array
    {
        return self::$logTrack;
    }

    public static function getErrorTrack(): array
    {
        return self::$errorTrack;
    }

    public static function getAll()
    {
        return array_merge(self::$logTrack, self::$errorTrack);
    }

    public static function addException(Throwable $e, $previous = null)
    {
        $excPrevious = null;

        if (!is_null($previous) && $previous instanceof Throwable) {
            $excPrevious = [
                'type'    => 'exception',
                'message' => $previous->getMessage(),
                'code'    => $previous->getCode(),
                'file'    => $previous->getFile() . ':' . $e->getLine(),
            ];
        }

        $track[] = [
            'type'     => 'exception',
            'message'  => $e->getMessage(),
            'code'     => $e->getCode(),
            'file'     => $e->getFile() . ':' . $e->getLine(),
            'previous' => $excPrevious,
        ];
        self::addErrorTrack($track);
    }
}