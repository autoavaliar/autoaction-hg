<?php

declare(strict_types=1);

namespace AutoAction\Hg\Translate;

/**
 * Configurações para a tradução
 *
 * @package AutoAction\Hg\Translate
 * @date    30/04/2020 11:25
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class HgTranslateConfig
{
    private $systemId;
    private $instanceId;
    private $ttl = (60 * 5);
    private $deleteCache=false;

    public function getSystemId()
    {
        return $this->systemId;
    }

    /**
     * Informe o ID do sistema
     * @param int $systemId
     */
    public function setSystemId(int $systemId)
    {
        $this->systemId = $systemId;
        return $this;
    }

    public function getTtl()
    {
        return $this->ttl;
    }

    /**
     * Informe em segundos, quanto tempo terá a lista de strings no cache
     * @param int $ttl tempo de vida da lista de strings
     */
    public function setTtl(int $ttl)
    {
        $this->ttl = $ttl;
        return $this;
    }

    public function getInstanceId()
    {
        return $this->instanceId;
    }


    public function setInstanceId(int $instanceId)
    {
        $this->instanceId = $instanceId;
        return $this;
    }

    /**
     * Verifica se o cache deve ser excluido
     * @return bool
     */
    public function isDeleteCache(): bool
    {
        return $this->deleteCache;
    }

    /**
     * @param bool $deleteCache
     */
    public function setDeleteCache(bool $deleteCache)
    {
        $this->deleteCache = $deleteCache;
        return $this;
    }
}