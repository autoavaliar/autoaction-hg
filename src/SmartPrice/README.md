# Projeto: AutoAction HG-SMART-PRICE

Este projeto tem como objetivo facilitar a integração com a HG SMART PRICE.

### Dependências

- Redis
- PHP 7.0 ou superior
- Ser um parceiro Auto Avaliar / Auto Action

### Como configurar o seu container de dependencia da HG?

```php
<?php
use AutoAction\Hg\HttpRequest;
use AutoAction\Hg\SmartPrice\HgSmartPriceClient;
use AutoAction\Utils\Cache\RedisCustom;

require 'vendor/autoload.php';

$hgSmartPrice = new HgSmartPriceClient($token, new HttpRequest('http://url.com.br'));
// passar sempre um array
$data = [
    'mileage' => 120000
    'cor' => 'PRETO'
    'boolean' => 0
    'version_id' => 46100
    'entity_id' => 101163092
    'state_id' => 25
]
try {
    $result = $hgSmartPrice->calculate($data);
    var_dump($result);
} catch (Exception $e) {
    print $e->getMessage();
}


```

### Como utilizar o pacote HG-SMART-PRICE ?

```php
$di->setShared(
    'HgSmartPrice',
    function ($token) use ($config) {
        return new HgSmartPriceClient($token, new HttpRequest('http://url.com.br'));
    }
);

```
