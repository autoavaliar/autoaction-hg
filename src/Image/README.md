# Projeto: AutoAction HG-IMAGE

Este projeto tem como objetivo facilitar a integração com a HG IMAGE.

### Dependências

- Redis
- PHP 7.0 ou superior
- Ser um parceiro Auto Avaliar / Auto Action

### Como configurar o seu container de dependencia da HG?

```php
<?php
use AutoAction\Hg\HttpRequest;
use AutoAction\Hg\Image\HgImageClient;
use AutoAction\Utils\Cache\RedisCustom;

require 'vendor/autoload.php';



$hgImage = new HgImageClient($token, new HttpRequest('http://url.com.br'));
// passar sempre um array
$images = [
    "https://storage.googleapis.com/photo-ecossistema/originals/usbi/109716_101162227_7542756_3.jpg"
];
$result = $hgImage->addWatermark($images);
try {
    $result = $hgImage->addWatermark($images);
    var_dump($result);
} catch (Exception $e) {
    print $e->getMessage();
}


```

### Como utilizar o pacote HG-IMAGEM ?

```php
$di->setShared(
    'HgImage',
    function ($token) use ($config) {

        return new HgImageClient($token, new HttpRequest('http://url.com.br'));
    
    }
);

```
