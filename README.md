# Projeto: AutoAction HG

Este projeto tem como objetivo facilitar a integração com a HG API.

### Dependências

- Redis
- PHP 7.0 ou superior
- Ser um parceiro Auto Avaliar / Auto Action

### Como configurar o seu container de dependencia da HG?

```php
<?php
use AutoAction\Hg\HgClient;
use AutoAction\Hg\HgConfig;
use AutoAction\Utils\Cache\RedisCustom;

require 'vendor/autoload.php';

// A ideia é usar estas configurações em um container de dependencia do seu projeto

// - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Redis
// - - - - - - - - - - - - - - - - - - - - - - - - - -
$redis = new RedisCustom();

// alguns redis não utiliza senha
if('{SE_REDIS_SENHA}'){
   $redis->auth('{REDIS_SENHA}');
}

$redis->connect('{REDIS_HOST}', '{REDIS_PORTA}');

// - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Configuração
// - - - - - - - - - - - - - - - - - - - - - - - - - -
$config = new HgConfig('NOME_DO_SEU_PROJETO', $redis);
$config->setCountryId('ID_DO_PAIS');
$config->setHost('HOST_DA_HG');
$config->setPartnerUser('USUARIO_DO_PARCEIRO');
$config->setPartnerPassword('SENHA_DO_PARCEIRO');

// - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Client
// - - - - - - - - - - - - - - - - - - - - - - - - - -
$client = new HgClient($config);

// a ideia é que seu container de dependencia retorne o HgClient
```

### Como utilizar o pacote HG?

```php
// um exemplo de um container de dependencia
$client = $this->di->getHgClient();

// recuperando o token de parceiro
$partnerToken = $client->getPartnerToken();

// fazendo o login de um usuário
$user = $client->getUserData('USUARIO', 'SENHA');

// chamando um endpoint qualquer na HG
$config = $client->post('ENDPOINT', ['PARAMETRO' => 'VALOR'], ['PARAMETRO_HEADER','VALOR_HEADER']);

```

```php
// exemplo de uso para traduções
use AutoAction\Hg\Translate\HgTranslateConfig;
use AutoAction\Hg\Translate\HgTranslate;
use AutoAction\Hg\Translate\HgLocalStringSample;

$client = $this->di->getHgClient();
$config = new HgTranslateConfig();
$config->setSystemId('ID_DO_SISTEMA');
$config->setInstanceId('ID_DA_INSTANCIA');

$translate = HgTranslate::getInstance($config, $client, new HgLocalStringSample);
$string = $translate->get('ROOT_DA_STRING');
```
 ---

# Vai contribuir com o projeto?

Que legal que vai contribuir, então segue as informações para poder rodar o docker do projeto.

### Informações Docker

 Adicione no seu arquivo de hosts
 
 ```127.0.0.1   web.local```
 
 Normalmente este arquivo fica em: `/etc/hosts`
 
 Para iniciar o container rode o comando:
 
 `docker-compose up -d`
 
 ### Acessando a URL do projeto
 
 Adicione no seu navegador ou postman `http://web.local:8199`
 
 ### Configurando o Xdebug no PhpStorm
 
 Vá no menu de configuração `File > Settings... > Languagens & Frameworks > PHP > Debug`
 
 Na seção `Xdebug` no campo `Debug port:` coloque a porta `10099`
 
 Também é preciso mapear as pastas do container
 
 Vá no menu de configuração `File > Settings... > Languagens & Frameworks > PHP > Servers`
 
 ### Outros comandos Docker
 
 Inicia Docker: `docker-compose up -d`
 
 Encerrar Docker: `docker-compose down`
 
 Levantar o docker com rebuild do container: `docker-compose up -d --build`
 
 Entrar no shell do container: `docker exec -it autoaction-hg-phpfpm /bin/sh`
 
 Verificar os logs de acesso do nginx: `docker logs -tf autoaction-hg-web`
 
 