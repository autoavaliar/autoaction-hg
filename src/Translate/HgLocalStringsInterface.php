<?php

declare(strict_types=1);

namespace AutoAction\Hg\Translate;

/**
 * Lista de strings locais para tradução do sistema
 *
 * @package AutoAction\Hg\Translate
 * @date    30/04/2020 10:02
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
interface HgLocalStringsInterface
{
    /**
     * Lista de strings local, esta lista é uma contingencia
     * para o caso do sistema não conseguir buscar na API a lista de tradução
     *
     * Exemplo de uma lista de strings
     *
     * ```
     * return [
     *    'root'  => 'texto padrao',
     *    'root2' => 'texto padrao 2'
     * ];
     * ```
     *
     * @return array
     * @see Markdown
     *
     */
    public function getStrings(): array;
}