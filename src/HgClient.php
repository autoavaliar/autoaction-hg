<?php

declare(strict_types=1);

namespace AutoAction\Hg;

use AutoAction\Hg\Message\LoggerStack;
use AutoAction\Utils\Cache\CacheInterface;
use DateTime;
use Exception;

/**
 * Cliente HG
 *
 * @package AutoAction\Hg
 * @date    29/04/2020 14:38
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class HgClient
{
    /**
     * @var HgRequest
     */
    private $request;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var HgConfig
     */
    private $config;

    /**
     * Inicialização do Client HG
     * @param HgConfig $config
     */
    public function __construct(HgConfig $config)
    {
        $this->request = new HgRequest($config);
        $this->config  = $this->request->getConfig();
        $this->cache   = $this->config->getCache();
    }

    /**
     * Configurações HG
     * @return HgConfig
     */
    public function getConfig(): HgConfig
    {
        return $this->config;
    }

    public function getPartnerToken()
    {
        $key = $this->config->getCacheKey('partner-token');

        if ($this->cache->exists($key)) {
            LoggerStack::addTrack(['PartnerTokenDataFrom'=>'cache']);
            return $this->cache->get($key);
        }

        try {
            $data = [
                'login'     => $this->config->getPartnerUser(),
                'password'  => $this->config->getPartnerPassword(),
                'goal'      => $this->config->getGoal(),
                'countryId' => $this->config->getCountryId()
            ];

            LoggerStack::addTrack(['PartnerDataLogin'=>$data]);

            $post = $this->request->request('partner/login', $data);

            $now       = new DateTime();
            $shelfLife = new DateTime($post['shelf_life']);
            $shelfLife->modify('-1 day');
            $ttl = ($shelfLife->getTimestamp() - $now->getTimestamp());

        } catch (Exception $e) {
            LoggerStack::addException($e);
            throw $e;
        }

        $this->cache->save($key, $post['token'], $ttl);

        return $post['token'];
    }

    /**
     * @throws Exception
     */
    public function getUserData($user, $password)
    {
        $key = $this->config->getCacheKey('user-' . $user . ':password-' . hash('sha512', $password));

        if ($this->cache->exists($key)) {
            LoggerStack::addTrack(['UserDataFrom'=>'cache']);
            return $this->cache->get($key);
        }

        try {
            $data = [
                'email'    => $user,
                'password' => $password,
            ];

            LoggerStack::addTrack(['UserLoginData'=>$data]);

            $post = $this->request->request('auth/login', $data, ['token' => $this->getPartnerToken()]);

            $now       = new DateTime();
            $shelfLife = new DateTime();
            $shelfLife->modify('+20 hour');
            $ttl = ($shelfLife->getTimestamp() - $now->getTimestamp());

        } catch (Exception $e) {
            LoggerStack::addException($e);
            throw $e;
        }

        $this->cache->save($key, $post, $ttl);

        return $post;
    }

    public function post(string $endpoint, array $data = [], array $header = [])
    {
        try {
            $header = array_merge(['token' => $this->getPartnerToken()], $header);
            return $this->request->request($endpoint, $data, $header);
        } catch (Exception $e) {
            LoggerStack::addException($e);
            throw $e;
        }
    }
}