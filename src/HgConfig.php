<?php

declare(strict_types=1);

namespace AutoAction\Hg;

use AutoAction\Utils\Cache\CacheInterface;
use Exception;

/**
 * Configurações basicas para a HG
 *
 * @package AutoAction\Hg
 * @date    29/04/2020 13:29
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class HgConfig
{
    /**
     * @var string Host da HG
     */
    private $host;

    /**
     * @var int
     */
    private $countryId;

    /**
     * @var string Usuário parceiro na HG
     */
    private $partnerUser;

    /**
     * @var string Senha do Parceiro
     */
    private $partnerPassword;

    /**
     * @var string
     */
    private $goal;

    /**
     * @var string
     */
    private $project;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param string         $project Nome do projeto
     * @param CacheInterface $cache   Instancia do cache
     */
    public function __construct(string $project, CacheInterface $cache)
    {
        $this->project = trim($project);
        $this->cache   = $cache;

        // pega o valor padrão nome do projeto, caso o cliente não informe um goal
        $this->goal    = $this->project;
    }

    public function getCache(): CacheInterface
    {
        return $this->cache;
    }

    public function getCacheKey($key)
    {
        if (empty($key)) {
            throw new Exception('Cache Key not found!', E_USER_ERROR);
        }

        return 'package:hg:' . $this->project . ':country-' . $this->getCountryId() . ':' . $key;
    }

    public function getHost(): string
    {
        if (is_null($this->host)) {
            throw new Exception('Host not found!', E_USER_ERROR);
        }

        return $this->host;
    }

    public function setHost(string $host)
    {
        $this->host = $host;
        return $this;
    }

    public function getPartnerUser(): string
    {
        if (is_null($this->partnerUser)) {
            throw new Exception('Partner user not found!', E_USER_ERROR);
        }

        return $this->partnerUser;
    }

    public function setPartnerUser(string $partnerUser)
    {
        $this->partnerUser = $partnerUser;
        return $this;
    }

    public function getPartnerPassword(): string
    {
        if (is_null($this->partnerPassword)) {
            throw new Exception('Partner password not found!', E_USER_ERROR);
        }

        return $this->partnerPassword;
    }

    public function setPartnerPassword(string $partnerPassword)
    {
        $this->partnerPassword = $partnerPassword;
        return $this;
    }

    public function getCountryId(): int
    {
        if (is_null($this->countryId)) {
            throw new Exception('Country ID not found!', E_USER_ERROR);
        }
        return $this->countryId;
    }

    public function setCountryId(int $countryId)
    {
        $this->countryId = $countryId;
        return $this;
    }

    public function getGoal(): string
    {
        return $this->goal;
    }

    public function setGoal(string $goal)
    {
        $this->goal = $goal;
        return $this;
    }
}