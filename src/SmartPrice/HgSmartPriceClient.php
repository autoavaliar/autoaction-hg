<?php

declare(strict_types=1);

namespace AutoAction\Hg\SmartPrice;

use AutoAction\Hg\HttpRequest\HttpRequest;
use AutoAction\Hg\Message\LoggerStack;
use Exception;

/**
 * Cliente HG Smart Price
 *
 * @package AutoAction\Hg
 *
 */
class HgSmartPriceClient
{
    const CALCULATE_ENDPOINT = "/smart-price/calculate";
    private $httpRequest;
    private $token;

    public function __construct(string $token, HttpRequest $httpRequest)
    {
        $this->token = $token;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @throws Exception
     */
    public function calculate(array $data = [], bool $logs = false, array $header = []): array
    {
        $endpoint = self::CALCULATE_ENDPOINT;
        if ($logs) {
            $endpoint .= '?log=1';
        }
        return $this->request($endpoint, $data, $header);
    }

    /**
     * @throws Exception
     */
    public function request(string $endpoint, array $data = [], array $header = [], $method = 'POST'): array
    {
        try {
            $header = array_merge(['token' => $this->getToken()], $header);

            $this->httpRequest->noCheckStatusCode();
            return $this->httpRequest->request($endpoint, $data, $header, $method);
        } catch (Exception $e) {
            LoggerStack::addException($e);
            throw $e;
        }
    }

    private function getToken(): string
    {
        return $this->token;
    }
}