FROM php:7.1-fpm-alpine

RUN apk add --no-cache --update make gcc g++ \
    libc-dev \
    autoconf \
    zlib-dev \
    linux-headers

# - copy composer executable
COPY --from=composer /usr/bin/composer /usr/bin/

# - docker-php-ext-install
RUN docker-php-ext-install mysqli \
    && docker-php-ext-install pdo \
    && docker-php-ext-install pdo_mysql

# - pecl install
RUN pecl install -o -f xdebug \
    && pecl install -o -f redis \
    && pecl install -o -f grpc \
    && pecl install -o -f protobuf

# - enable extensions
RUN docker-php-ext-enable xdebug redis grpc protobuf

# - php.ini files and configurations
ENV XDEBUG_INI_FILE=/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_port=10099" >> ${XDEBUG_INI_FILE} \
    && echo "xdebug.coverage_enable=0" >> ${XDEBUG_INI_FILE} \
    && echo "xdebug.remote_enable=1" >> ${XDEBUG_INI_FILE} \
    && echo "xdebug.remote_connect_back=1" >> ${XDEBUG_INI_FILE} \
    && echo "xdebug.remote_log=/tmp/xdebug.log" >> ${XDEBUG_INI_FILE} \
    && echo "xdebug.remote_autostart=true" >> ${XDEBUG_INI_FILE} \
    && echo "xdebug.remote_host=webserver" >> ${XDEBUG_INI_FILE}